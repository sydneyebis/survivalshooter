﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class introScript : MonoBehaviour {
    float timer;
    public GameObject text;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer > 1) {
            showText();
        }
        if (timer > 4) {
            removeText();
        }
	}
    void removeText()
    {
        text.transform.DOMoveX(-300, 1);
    }
    void showText()
    {
        text.transform.DOMoveY(250, 1);
    }
}
