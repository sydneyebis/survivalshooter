﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public GameObject player;
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
    public Image weaponImage;
    float timer;
    float switchtimer;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;
    int[] weapons = new int[] { 0, 1, 2};
    public int selectedWeapon;
    float maxDistance = 30;
    RaycastHit[] hit;
    // -----------------------------------------------------------------------------
    public Sprite autoRifle;
    public Sprite rifle;
    public Sprite grenade;
    //------------------------------------------
    void Awake()
    {
        //player = GameObject.Find("player");
        selectedWeapon = weapons[0];
        shootableMask = LayerMask.GetMask("Shootable");
        gunParticles = GetComponent<ParticleSystem>();
        gunLine = GetComponent<LineRenderer>();
        gunAudio = GetComponent<AudioSource>();
        gunLight = GetComponent<Light>();
    }


    void Update()
    {
        timer += Time.deltaTime;
        switchtimer += Time.deltaTime;
        if (Input.GetButton("Fire2")&& switchtimer >= .25)
        {
			if (selectedWeapon != (weapons.Length - 1)) {
				selectedWeapon += 1;
				pictureChanger (selectedWeapon);
                switchtimer = 0;
			} else {
				selectedWeapon = weapons [0];
				pictureChanger (selectedWeapon);
                switchtimer = 0;
			}
        }
        if (selectedWeapon == 0)
        {
            damagePerShot = 30;
            timeBetweenBullets = 0.15f;
        }
        if (selectedWeapon == 1)
        {
            damagePerShot = 200;
            timeBetweenBullets = 1f;
        }
        if (selectedWeapon == 2)
        {
            damagePerShot = 50;
            timeBetweenBullets = .7f;
        }
        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && selectedWeapon != 2)
        {
            Shoot();
        }
        if (Input.GetButton("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0 && selectedWeapon == 2)
        {
            Shotgun();
        }
        if (timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects();
        }
        /*if (Input.GetKeyDown("b")) {
            GameObject grenade = Instantiate(bomb, player.transform.position, player.transform.rotation);
        }*/
    }

    void Shotgun()
    {
        hit = Physics.SphereCastAll(player.transform.position, 1.75f, player.transform.forward, 5, shootableMask);
		timer = 0f;
        var manyGunLines = new LineRenderer[hit.Length];
		var points = new Vector3[hit.Length];
        
        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();
        

        //gunLine.SetPosition(0, transform.position);

        /*shootRay.origin = transform.position;
        shootRay.direction = transform.forward;*/
        Debug.Log(hit);
        for(int i = 0; i<hit.Length; i++){
			
            if (hit[i].collider.CompareTag("enemy"))
            {
				points [i] = hit [i].collider.gameObject.transform.position;
                //manyGunLines[i] = Instantiate(GetComponent<LineRenderer>(), player.transform); 
               // manyGunLines[i].enabled = true;
                EnemyHealth enemyHealth = hit[i].collider.GetComponent<EnemyHealth>();
                if (enemyHealth != null)
                {
					//points[i] = new Vector3(i * 0.5f, Mathf.Sin(i + t), 0.0f);
                    enemyHealth.TakeDamage(damagePerShot, hit[i].point);
                   // manyGunLines[i].SetPosition (i, points[i]);

					//gunLine.SetPositions(i, hit[i]);
                }

            }
        }
        Array.Clear(manyGunLines, 0, manyGunLines.Length);
    }
    public void DisableEffects()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot()
    {
        timer = 0f;

        gunAudio.Play();

        gunLight.enabled = true;

        gunParticles.Stop();
        gunParticles.Play();

        gunLine.enabled = true;
        gunLine.SetPosition(0, transform.position);

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if (Physics.Raycast(shootRay, out shootHit, range, shootableMask))
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent<EnemyHealth>();
            if (enemyHealth != null)
            {
                enemyHealth.TakeDamage(damagePerShot, shootHit.point);
            }
            gunLine.SetPosition(1, shootHit.point);
        }
        else
        {
            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
        }
    }
    public void pictureChanger(int number)
    {
        if (number == 0)
        {
            weaponImage.sprite = autoRifle;
        }
        if (number == 1)
        {
            weaponImage.sprite = rifle;
        }
        if (number == 2) {
            weaponImage.sprite = grenade;
        }
    }
}
